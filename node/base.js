"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const got = require("got");
/** Base fetch, nothing special to see here */
exports.default = (method, url, options, MidManager) => __awaiter(this, void 0, void 0, function* () {
    const request = MidManager.execute('OnBeforeRequest', Object.assign({}, options, { method }));
    return (got(`${options.baseUrl || ''}${url}`, request)
        .then((response) => MidManager.execute('OnFetchFulfilled', response))
        .catch((error) => MidManager.execute('OnFetchError', error)));
});
//# sourceMappingURL=base.js.map