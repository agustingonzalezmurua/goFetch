import { InstanceMiddlewares } from '../core/middleware-manager';
import got = require('got');
export declare type RequestOptions = Request & got.GotOptions<string>;
export interface Request {
    baseUrl?: string;
    body?: any;
    /**
     * If set to true and `Content-Type` header is not set, it will be set to `application/json`.
     *
     * Parse response body with `JSON.parse` and set accept header to `application/json`. If used in conjunction with the form option, the body will the stringified as querystring and the response parsed as `JSON`.
     * @default false
     */
    json?: boolean;
    /**
     * If set to true and Content-Type header is not set, it will be set to application/x-www-form-urlencoded.
     *
     * > **Note**: body must be a plain object.
     */
    form?: boolean;
}
declare const _default: (options?: RequestOptions) => {
    get: (url: string, options?: RequestOptions) => Promise<got.Response<any>>;
    delete: (url: string, options?: RequestOptions) => Promise<got.Response<any>>;
    post: (url: string, options?: RequestOptions) => Promise<got.Response<any>>;
    put: (url: string, options?: RequestOptions) => Promise<got.Response<any>>;
    patch: (url: string, options?: RequestOptions) => Promise<got.Response<any>>;
    register: (middlewares: InstanceMiddlewares<RequestOptions>) => void;
};
export default _default;
