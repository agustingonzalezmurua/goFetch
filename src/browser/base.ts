import { Middlewares } from "../core/middlewares"
import { Method } from "../core/method"
import { MiddlewareManager } from '../core/middleware-manager'
import { Request } from "./index";

/** Base fetch, nothing special to see here */
export default (method: Method, url: string, options: Request, MidManager: MiddlewareManager<Request>) : Promise<Response> => {
  const request = MidManager.execute('OnBeforeRequest', {
    ...options,
    method
  })
  return (
    fetch(`${options.baseUrl || '' }${url}`, request)
      .then((response) => MidManager.execute('OnFetchFulfilled', response))
      .catch((error) => MidManager.execute('OnFetchError', error))
  )
}
