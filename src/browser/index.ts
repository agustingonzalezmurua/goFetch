import { Middlewares } from '../core/middlewares'
import { InstanceMiddlewares, MiddlewareManager } from '../core/middleware-manager'
import FetchFunctions from '../core/call'
import baseFetch from './base'

export interface Request extends RequestInit {
  baseUrl?: string
  body?: any
}

export default (options?: Request) => {
  const middlewareManager = new MiddlewareManager<Request>()
  return FetchFunctions<Request, Response>(baseFetch, middlewareManager)(options)
}
