import { Middlewares } from '../core/middlewares'
import { InstanceMiddlewares, MiddlewareManager } from '../core/middleware-manager'
import FetchFunctions from '../core/call'
import baseFetch from './base'
import got = require('got');

export type RequestOptions = Request & got.GotOptions<string>

export interface Request {
  baseUrl?: string
  body?: any
  /**
   * If set to true and `Content-Type` header is not set, it will be set to `application/json`.
   * 
   * Parse response body with `JSON.parse` and set accept header to `application/json`. If used in conjunction with the form option, the body will the stringified as querystring and the response parsed as `JSON`.
   * @default false
   */
  json?: boolean
  /**
   * If set to true and Content-Type header is not set, it will be set to application/x-www-form-urlencoded.
   * 
   * > **Note**: body must be a plain object.
   */
  form?: boolean
}

export default (options?: RequestOptions) => {
  const middlewareManager = new MiddlewareManager<RequestOptions>()
  return FetchFunctions<RequestOptions, got.Response<any>>(baseFetch, middlewareManager)(options)
}
