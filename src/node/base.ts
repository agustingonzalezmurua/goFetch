import { Middlewares } from "../core/middlewares"
import { Method } from "../core/method"
import { MiddlewareManager } from '../core/middleware-manager'
import { Request } from "./index";
import * as got from 'got'

/** Base fetch, nothing special to see here */
export default async (method: Method, url: string, options: Request, MidManager: MiddlewareManager<Request>): Promise<got.Response<any>> => {
  const request = MidManager.execute('OnBeforeRequest', {
    ...options,
    method
  })

  return (
    got(`${options.baseUrl || ''}${url}`, request)
      .then((response) => MidManager.execute('OnFetchFulfilled', response))
      .catch((error) => MidManager.execute('OnFetchError', error))
  )
}
