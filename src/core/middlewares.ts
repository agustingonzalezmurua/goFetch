export type Middleware<entry, exit> = (value: entry) => Promise<exit>
export type OnBeforeRequest<R> = Middleware<R, R>
export type OnFetchFulfilled = Middleware<Response, any>
export type OnFetchError = Middleware<any, any>

type DataMiddlewares<R> = {
  OnBeforeRequest?: OnBeforeRequest<R>
}

type FetchMiddlewares = {
  OnFetchFulfilled?: OnFetchFulfilled
  OnFetchError?: OnFetchError
}

export type Middlewares<R> = DataMiddlewares<R> & FetchMiddlewares
