import { Middlewares, OnBeforeRequest, OnFetchError, OnFetchFulfilled } from './middlewares';
import { MiddlewareManager, InstanceMiddlewares } from './middleware-manager';
import { Method } from './method'
import got = require('got');

export type FetchFunction<Request, Response> = (method: Method, url: string, options: Request, middlewareManagerInstance: MiddlewareManager<Request>) => Promise<Response>

/** Fetch-calls */
export default <Req, Res>(baseFetch: FetchFunction<Req, Res>, middlewareManager: MiddlewareManager<Req>) => (initParameters: Req) => {
  const mergeOptions = (
    contextual: Req,
    parametric: Req
  ): Req => {
    return { ...contextual, ...parametric }
  }
  return ({
    /** Creates a fetch as GET, `options.data` */
    get: (url: string, options?: Req) => (
      baseFetch('GET', url, mergeOptions(initParameters, options), middlewareManager)
    ),
    /** Creates a fetch as DELETE, `options.data` is ignored */
    delete: (url: string, options?: Req) => (
      baseFetch('DELETE', url, mergeOptions(initParameters, options), middlewareManager)
    ),
    /** Executes a `POST` request */
    post: (url: string, options?: Req) => (
      baseFetch('POST', url, mergeOptions(initParameters, options), middlewareManager)
    ),
    /** Executes a `PUT` request */
    put: (url: string, options?: Req) => (
      baseFetch('PUT', url, mergeOptions(initParameters, options), middlewareManager)
    ),
    /** Executes a `PATCH` request */
    patch: (url: string, options?: Req) => (
      baseFetch('PATCH', url, mergeOptions(initParameters, options), middlewareManager)
    ),
    register: (middlewares: InstanceMiddlewares<Req>) => middlewareManager.register(middlewares)
  })
}