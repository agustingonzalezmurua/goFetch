import { OnBeforeRequest, OnFetchError, OnFetchFulfilled, Middlewares, Middleware } from './middlewares'

export type InstanceMiddlewares<RequestParameters> = {
  OnBeforeRequest?: OnBeforeRequest<RequestParameters>[],
  OnFetchError?: OnFetchError[],
  OnFetchFulfilled?: OnFetchFulfilled[],
}

export class MiddlewareManager<RequestParameter> {
  public middlewares: InstanceMiddlewares<RequestParameter> = {
    OnBeforeRequest: [],
    OnFetchError: [],
    OnFetchFulfilled: []
  }

  constructor(middlewares?: InstanceMiddlewares<RequestParameter>) {
    this.register(middlewares)
  }
  
  register = (middlewares?: InstanceMiddlewares<RequestParameter>) => {
    if (!middlewares) return
    Object.keys(middlewares).forEach((key: keyof InstanceMiddlewares<RequestParameter>) => {
      if (!this.middlewares[key]) return
      this.middlewares[key] = [...this.middlewares[key], ...middlewares[key]]
    })
  }
  
  execute = (key: keyof InstanceMiddlewares<RequestParameter>, parameter: any): any => {
    const middleware = this.middlewares[key] as Middleware<any, any>[]
    if (!middleware || middleware.length !== 0) {
      middleware.forEach(async (middleware) => {
        parameter = await middleware(parameter)
      })
    }
    return parameter
  }
}
