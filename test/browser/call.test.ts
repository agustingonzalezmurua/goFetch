import 'whatwg-fetch' // Fetch polyfill for window.fetch
import GoFetch from '../../src/browser'
const goFetch = GoFetch({
  baseUrl: 'https://jsonplaceholder.typicode.com/posts',
  headers: { 'token-x': 'supersecrettoken' }
})

test('get successfully', async () => {
  expect.assertions(1)
  const response = await goFetch.get('/1')
  expect(response.ok).toBe(true)
})

test('get, parse and evaluate object', async () => {
  expect.assertions(1);
  const response = await goFetch.get('/1')
  const data = await response.json()
  expect(data.id).toBe(1)
})

test('post successfully', async () => {
  expect.assertions(1)
  const data =  { title: 'Foo!' }
  const response = await goFetch.post('/', { body: JSON.stringify(data) })
  expect(response.ok).toBe(true)
})

test('put successfully', async () => {
  expect.assertions(1)
  const data = { id: 1, title: 'Foo!' }
  const response = await goFetch.put('/1', { body: JSON.stringify(data) })
  expect(response.ok).toBe(true)
})

test('delete successfully', async () => {
  expect.assertions(1)
  const response = await goFetch.delete('/1')
  expect(response.ok).toBe(true)
})

test('404', async () => {
  expect.assertions(1)
  const response = await goFetch.get('/1/1')
  expect(response.status).toBe(404)
})
