import 'whatwg-fetch' // Fetch polyfill for window.fetch
import GoFetch from '../../src/node'
const goFetch = GoFetch({
  baseUrl: 'https://jsonplaceholder.typicode.com/posts',
  headers: { 'token-x': 'supersecrettoken' },
  json: true
})

test('get successfully', async () => {
  expect.assertions(1)
  const response = await goFetch.get('/1')
  expect(response.statusCode).toBe(200)
})

test('get, parse and evaluate object', async () => {
  expect.assertions(1);
  const response = await goFetch.get('/1')
  const data = await response.body
  expect(data.id).toBe(1)
})

test('post successfully', async () => {
  expect.assertions(1)
  const data =  { title: 'Foo!' }
  const response = await goFetch.post('/', { body: data })
  expect(response.statusCode).toBe(201)
})

test('put successfully', async () => {
  expect.assertions(1)
  const data = { id: 1, title: 'Foo!' }
  const response = await goFetch.put('/1', { body: data })
  expect(response.statusCode).toBe(200)
})

test('delete successfully', async () => {
  expect.assertions(1)
  const response = await goFetch.delete('/1')
  expect(response.statusCode).toBe(200)
})

test('404', async () => {
  expect.assertions(1)
  const response = await goFetch.get('/1/1')
  expect(response.statusCode).toBe(404)
})
