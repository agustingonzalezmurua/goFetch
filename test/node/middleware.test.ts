import 'whatwg-fetch' // Fetch polyfill for window.fetch
import GoFetch from '../../src/browser'
const url = 'https://jsonplaceholder.typicode.com/posts'
const goFetch = GoFetch({
  headers: { "content-type": "application/json; charset=utf-8" },
})

// Register valid middlewares
goFetch.register({
  OnBeforeRequest: [
    async (request) => {
      if (request.body && request.method === 'POST') {
        request.body = JSON.stringify(request.body)
      }
      expect(request).toBeDefined()
      return request
    }
  ],
  OnFetchFulfilled: [
    async (response) => {
      expect(response).toBeDefined();
      if (response.body) {
        return response.json();
      }
      return response;
    }
  ],
  OnFetchError: [
    async (error) => {
      expect(error).toBeDefined();
    }
  ]
})

// Register unknown events
goFetch.register({ })

test('get successfully', async () => {
  expect.assertions(3)
  const response = await goFetch.get(url + '/1')
  expect(response.ok).toBe(true)
})

test('get 404', async () => {
  expect.assertions(3)
  const response = await goFetch.get(url + '/1/2')
  expect(response.ok).toBe(false)
})

test('post successfully', async () => {
  expect.assertions(3)
  const data = { title: 'Foo!' }
  const response = await goFetch.post(url, { body: data } )
  expect(response.ok).toBe(true)
})
