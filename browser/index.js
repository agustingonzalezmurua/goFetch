"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const middleware_manager_1 = require("../core/middleware-manager");
const call_1 = require("../core/call");
const base_1 = require("./base");
exports.default = (options) => {
    const middlewareManager = new middleware_manager_1.MiddlewareManager();
    return call_1.default(base_1.default, middlewareManager)(options);
};
//# sourceMappingURL=index.js.map