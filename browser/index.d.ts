import { InstanceMiddlewares } from '../core/middleware-manager';
export interface Request extends RequestInit {
    baseUrl?: string;
    body?: any;
}
declare const _default: (options?: Request) => {
    get: (url: string, options?: Request) => Promise<Response>;
    delete: (url: string, options?: Request) => Promise<Response>;
    post: (url: string, options?: Request) => Promise<Response>;
    put: (url: string, options?: Request) => Promise<Response>;
    patch: (url: string, options?: Request) => Promise<Response>;
    register: (middlewares: InstanceMiddlewares<Request>) => void;
};
export default _default;
