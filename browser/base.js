"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** Base fetch, nothing special to see here */
exports.default = (method, url, options, MidManager) => {
    const request = MidManager.execute('OnBeforeRequest', Object.assign({}, options, { method }));
    return (fetch(`${options.baseUrl || ''}${url}`, request)
        .then((response) => MidManager.execute('OnFetchFulfilled', response))
        .catch((error) => MidManager.execute('OnFetchError', error)));
};
//# sourceMappingURL=base.js.map