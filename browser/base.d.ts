import { Method } from "../core/method";
import { MiddlewareManager } from '../core/middleware-manager';
import { Request } from "./index";
declare const _default: (method: Method, url: string, options: Request, MidManager: MiddlewareManager<Request>) => Promise<Response>;
/** Base fetch, nothing special to see here */
export default _default;
