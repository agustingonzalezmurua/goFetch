"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class MiddlewareManager {
    constructor(middlewares) {
        this.middlewares = {
            OnBeforeRequest: [],
            OnFetchError: [],
            OnFetchFulfilled: []
        };
        this.register = (middlewares) => {
            if (!middlewares)
                return;
            Object.keys(middlewares).forEach((key) => {
                if (!this.middlewares[key])
                    return;
                this.middlewares[key] = [...this.middlewares[key], ...middlewares[key]];
            });
        };
        this.execute = (key, parameter) => {
            const middleware = this.middlewares[key];
            if (!middleware || middleware.length !== 0) {
                middleware.forEach((middleware) => __awaiter(this, void 0, void 0, function* () {
                    parameter = yield middleware(parameter);
                }));
            }
            return parameter;
        };
        this.register(middlewares);
    }
}
exports.MiddlewareManager = MiddlewareManager;
//# sourceMappingURL=middleware-manager.js.map