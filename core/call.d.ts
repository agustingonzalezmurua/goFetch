import { MiddlewareManager, InstanceMiddlewares } from './middleware-manager';
import { Method } from './method';
export declare type FetchFunction<Request, Response> = (method: Method, url: string, options: Request, middlewareManagerInstance: MiddlewareManager<Request>) => Promise<Response>;
declare const _default: <Req, Res>(baseFetch: FetchFunction<Req, Res>, middlewareManager: MiddlewareManager<Req>) => (initParameters: Req) => {
    /** Creates a fetch as GET, `options.data` */
    get: (url: string, options?: Req) => Promise<Res>;
    /** Creates a fetch as DELETE, `options.data` is ignored */
    delete: (url: string, options?: Req) => Promise<Res>;
    /** Executes a `POST` request */
    post: (url: string, options?: Req) => Promise<Res>;
    /** Executes a `PUT` request */
    put: (url: string, options?: Req) => Promise<Res>;
    /** Executes a `PATCH` request */
    patch: (url: string, options?: Req) => Promise<Res>;
    register: (middlewares: InstanceMiddlewares<Req>) => void;
};
/** Fetch-calls */
export default _default;
