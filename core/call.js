"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** Fetch-calls */
exports.default = (baseFetch, middlewareManager) => (initParameters) => {
    const mergeOptions = (contextual, parametric) => {
        return Object.assign({}, contextual, parametric);
    };
    return ({
        /** Creates a fetch as GET, `options.data` */
        get: (url, options) => (baseFetch('GET', url, mergeOptions(initParameters, options), middlewareManager)),
        /** Creates a fetch as DELETE, `options.data` is ignored */
        delete: (url, options) => (baseFetch('DELETE', url, mergeOptions(initParameters, options), middlewareManager)),
        /** Executes a `POST` request */
        post: (url, options) => (baseFetch('POST', url, mergeOptions(initParameters, options), middlewareManager)),
        /** Executes a `PUT` request */
        put: (url, options) => (baseFetch('PUT', url, mergeOptions(initParameters, options), middlewareManager)),
        /** Executes a `PATCH` request */
        patch: (url, options) => (baseFetch('PATCH', url, mergeOptions(initParameters, options), middlewareManager)),
        register: (middlewares) => middlewareManager.register(middlewares)
    });
};
//# sourceMappingURL=call.js.map