import { OnBeforeRequest, OnFetchError, OnFetchFulfilled } from './middlewares';
export declare type InstanceMiddlewares<RequestParameters> = {
    OnBeforeRequest?: OnBeforeRequest<RequestParameters>[];
    OnFetchError?: OnFetchError[];
    OnFetchFulfilled?: OnFetchFulfilled[];
};
export declare class MiddlewareManager<RequestParameter> {
    middlewares: InstanceMiddlewares<RequestParameter>;
    constructor(middlewares?: InstanceMiddlewares<RequestParameter>);
    register: (middlewares?: InstanceMiddlewares<RequestParameter>) => void;
    execute: (key: "OnBeforeRequest" | "OnFetchError" | "OnFetchFulfilled", parameter: any) => any;
}
