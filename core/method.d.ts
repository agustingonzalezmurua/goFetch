export declare type Method = 'GET' | 'POST' | 'DELETE' | 'PUT' | 'PATCH';
