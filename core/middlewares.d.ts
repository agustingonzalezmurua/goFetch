export declare type Middleware<entry, exit> = (value: entry) => Promise<exit>;
export declare type OnBeforeRequest<R> = Middleware<R, R>;
export declare type OnFetchFulfilled = Middleware<Response, any>;
export declare type OnFetchError = Middleware<any, any>;
declare type DataMiddlewares<R> = {
    OnBeforeRequest?: OnBeforeRequest<R>;
};
declare type FetchMiddlewares = {
    OnFetchFulfilled?: OnFetchFulfilled;
    OnFetchError?: OnFetchError;
};
export declare type Middlewares<R> = DataMiddlewares<R> & FetchMiddlewares;
export {};
